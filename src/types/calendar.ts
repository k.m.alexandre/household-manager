export type ReceiptSummary = {
    name: string,
    start: Date,
    end: Date,
    color: string
}

export type CalendarDate = {
    date: string,
    day: number,
    future: boolean,
    hasDay: boolean,
    hasTime: boolean,
    hour: number,
    minute: number,
    month: number,
    past: boolean,
    present: boolean,
    time: string,
    weekday: number,
    year: number
}

export type Receipt = {
    _id: string,
    amount: number,
    category: string,
    date: number,
    image: string
}

export type Range = {
    start: number,
    end: number
}

