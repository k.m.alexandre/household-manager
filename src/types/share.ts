export type PeerData = {
    address: string,
    hash: string,
    entry: string,
    progress: string,
    have: string
}

export type Peer = {
    id: string,
    addrs: Array<string>
}
