import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import i18n from './i18n'
import Notifications from 'vue-notification'
// import Web3 from "web3"

library.add(faPlus)

// Vue.prototype.ipfs = undefined;
// Vue.prototype.db = undefined;

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(Notifications)

Vue.config.productionTip = false

// @ts-ignore
// window.web3 = new Web3(Web3.givenProvider || "ws://localhost:8545");
// const Accounts = require('web3-eth-accounts');

// Passing in the eth or web3 package is necessary to allow retrieving chainId, gasPrice and nonce automatically
// for accounts.signTransaction().
// const accounts = new Accounts('ws://localhost:8546');
// console.log('ACCOUTNS ' , accounts)

// @ts-ignore
// window.LOG = 'orbit*'

new Vue({
  router,
  vuetify,
  i18n,
  render: h => h(App)
}).$mount('#app')
