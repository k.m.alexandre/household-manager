// @ts-ignore
export const encode = (data: object) => new TextEncoder().encode(
    JSON.stringify(data));

// @ts-ignore
export const decode = (data: object) => JSON.parse(new TextDecoder().decode(data));
