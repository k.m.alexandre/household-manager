import { action, computed, observable } from "mobx";
import { CalendarModel } from "./modules/calendar";
import { IpfsModel } from "./modules/ipfs";
import { Receipt } from "@/types/calendar";

// @ts-ignore
// window.LOG = "orbit*"

export class RootStore {
    calendar: CalendarModel
    @observable ipfs: IpfsModel
    @observable notificationPermission !: string
    privateKey!: CryptoKey
    publicKey!: CryptoKey
    db !: IDBDatabase

    // get indexedDB() {
    //     return window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB;
    // }

    @computed get initialized() {
        if (this.ipfs === undefined) return false;
        return this.ipfs.initialized;
    }

    constructor() {
        // this.indexedDB
        // window.crypto.subtle.generateKey({
        //     name: "RSA-OAEP",
        //     modulusLength: 4096,
        //     publicExponent: new Uint8Array([1, 0, 1]),
        //     hash: "SHA-256"
        // }, true, ["encrypt", "decrypt"]).then(async keyPair => {
        //     console.log("KEY PAIR ", keyPair)
        //     const publicKey = await window.crypto.subtle.exportKey(
        //         "jwk", keyPair.publicKey);

        //     console.log(publicKey)
        //     const privateKey = await window.crypto.subtle.exportKey(
        //         "jwk", keyPair.privateKey);
        //     console.log(privateKey)
        // });
        // window.crypto.subtle.generateKey({
        //     "name": "ECDSA",
        //     "namedCurve": "P-256"
        // }, true, ["sign", "verify"]).then(keys => {
        //     console.log("KEYS > ", keys)
        // })
        this.calendar = new CalendarModel(this)
        this.ipfs = new IpfsModel(this)
        this.ipfs.init()
        try {
        Notification.requestPermission().then(notificationPermission =>{
            this.notificationPermission = notificationPermission;
        });
        } catch (err) {
            console.log(
                "[Notification API error] Running Internet Explorer or Safari?", err)
        }
    }

    public encrypt(data: string, keys: any) {
        console.log('ENCRYPT > ', data, keys)
    }

    public decrypt(data: string, keys: any) {
        console.log('DECRYPT > ', data, keys)
    }

    // private openDB() {
    //     const request = this.indexedDB.open('householdExpenses', 2)
    //     request.onsuccess = (event) => {
    //         this.db = event.target.result;
    //         console.log("DB >> ", this.db)
    //     }
    // }
}
