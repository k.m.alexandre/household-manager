import { action, computed, observable, toJS } from "mobx";
import { RootStore } from "@/store"
// import * as IPFS from 'ipfs'
const IPFS = require("ipfs")
import OrbitDB from "orbit-db"
// import * as OrbitDB from "orbit-db";
import Store from "orbit-db-store";
import { User } from "@/types/user"
import { Receipt } from "@/types/calendar"
import { ReceiveData } from "@/store/modules/user"
import { encode } from "@/utils/message"


interface DocumentStore<T> extends Store {

    put(doc: T): Promise<string>;
    get(key: any): T[];

    query(mapper: (doc: T) => void): T[]

    del(key: any): Promise<string>;

}

export class IpfsModel {
    root: RootStore
    public node !: typeof IPFS
    public nodeInfo !: object
    public orbitdb !: OrbitDB
    public receiptDStore !: DocumentStore<Receipt>
    public userDStore !: DocumentStore<User>
    @observable public initialized: boolean = false
    @observable public replicated: boolean = false
    private subscriptions: Array<string> = [];

    constructor(root: RootStore) {
        this.root = root;
    }

    @computed get nodeId(): string {
        if (this.nodeInfo !== undefined) {
            // @ts-ignore
            return this.nodeInfo.id;
        } else {
            return "";
        }
    }

    @computed get receipts() {
        return this.receiptDStore;
    }

    @computed get userStore() {
        return this.userDStore;
    }

    @action.bound setReplicated(replicated: boolean) {
        this.replicated = replicated;
    }

    public async init() {
        try {
            this.node = await IPFS.create({
                start: true,
                EXPERIMENTAL: {
                    pubsub: true,
                },
                config: {
                    Bootstrap: [
                        '/dns6/talkwiz.me/tcp/4430/wss/p2p/12D3KooWAw84ockQoTVzhe9bnrb7AWQPJQ11B1xoE2NH3h1ncnm7',
                        '/dns4/talkwiz.me/tcp/4430/wss/p2p/12D3KooWAw84ockQoTVzhe9bnrb7AWQPJQ11B1xoE2NH3h1ncnm7'
                    ],
                    Addresses: {
                        Swarm: [
                            '/dns4/talkwiz.me/tcp/9091/wss/p2p-webrtc-star',
                            '/dns6/talkwiz.me/tcp/9091/wss/p2p-webrtc-star',
                            '/ip4/0.0.0.0/tcp/4001'
                            // '/ip4/0.0.0.0/tcp/4003/ws',
                            // '/dns4/talkwiz.me/tcp/4430/wss/p2p-webrtc-star'
                            // '/dns4/talkwiz.me/tcp/4430/p2p/12D3KooWAw84ockQoTVzhe9bnrb7AWQPJQ11B1xoE2NH3h1ncnm7/p2p-circuit/p2p/'
                            // '/dns4/star.talkwiz.me/tcp/9091/wss/p2p-webrtc-star',
                            // '/dns6/star.thedisco.zone/tcp/9090/wss/p2p-webrtc-star'
                        ],
                        API: '/ip4/127.0.0.1/tcp/4001',
                        Gateway: '/ip4/0.0.0.0/tcp/4001'
                    }
                }
            })

            // this.node = await IPFS.create();
            this.orbitdb = await OrbitDB.createInstance(this.node)
            // const defaultOptions = { accessController: { write: [this.orbitdb.identity.id] }}
            const receiptDStore = await this.orbitdb.docstore('receipts')
            await receiptDStore.load();
            this.receiptDStore = receiptDStore as DocumentStore<Receipt>;
            const userDStore = await this.orbitdb.docstore('users')
            await userDStore.load();
            this.userDStore = userDStore as DocumentStore<User>;
            // this.receiptDStore.events.on("replicate.progress", (address, hash, entry, progress, have) => {
            //     console.log("PROGRESS >>> ", address, hash, entry, progress, have)
            // })
            // this.receiptDStore.events.on("peer", peer => {
            //     console.log("PEER >> ", peer)
            // })
            // this.receiptDStore.events.on("peer.exchanged", (peer, address, heads) => {
            //     console.log("PEER EXCHANGED >> >", peer, address, heads)
            // }
            this.nodeInfo = await this.node.id();

            // await this.node.pubsub.subscribe(this.nodeInfo.id, this.handleMessageReceived.bind(this))

            // const msg = new TextEncoder().encode('banana')

            // await this.node.pubsub.publish(topic, msg)

            // msg was broadcasted
            // console.log(`published to ${topic}`)
            this.initialized = true;
        } catch (err) {
            alert(err)
            // console.log(err)
            // @ts-ignore
            this.initialized = undefined;
        }
    }

    public handlePeerConnected (ipfsPeer: any) {
        console.log("HANDLE PEER CONNECTED >> ", ipfsPeer)
        const ipfsId = ipfsPeer.id.toB58String()
        console.log("IPFS ID >> ", ipfsId)
        // setTimeout(async () => {
            // await this.sendMessage(ipfsId, { userDb: user.id })
        // }, 2000)
        // if (this.onpeerconnect) this.onpeerconnect(ipfsPeer)
    }

    public async handleMessageReceived (msg: any) {
        console.log("HANDLE MESSAGE RECEIVED >> ", msg)
        const parsedMsg = JSON.parse(msg.data.toString())
        const msgKeys = Object.keys(parsedMsg)
        switch (msgKeys[0]) {
            case 'userDb': {
                console.log("MSG KEY ", msgKeys[0])
                console.log("PARSED MSG > ", parsedMsg)
                const peerDb = await this.orbitdb.open(parsedMsg.userDb)
                peerDb.events.on('replicated', async () => {
                    console.log("REPLICATED >>>> ", parsedMsg.userDb)
                    }
                )
            }
                break
            default:
                break
        }
        // if (this.onmessage) this.onmessage(msg)
    }

    public publish(topic: string, message: object) {
        try {
            console.log("Publish @" + topic, " msg: " + message);
            this.node.pubsub.publish(topic, encode(message));
        } catch (err) {
            console.log("IFPS node not yet instantiated: ", err);
        }
    }

    public subscribe(topic: string, receivedDataHandler: ReceiveData) {
        try {
            console.log("Subscribe @" + topic)
            const isAlreadySubscribed = this.subscriptions.findIndex(
                subscription => topic === subscription);
            if (isAlreadySubscribed > -1) return
            this.node.pubsub.subscribe(topic, receivedDataHandler)
            this.node.pubsub.ls().then((subscriptions: string[]) => {
                this.subscriptions = subscriptions;
            });
        } catch (err) {
            console.log("IFPS node not yet instantiated: ", err);
        }
    }

    public unsubscribe(topic: string) {
        try {
            console.log("UnSubscribe from " + topic)
            this.node.pubsub.unsubscribe(topic);
            this.subscriptions.splice(this.subscriptions.indexOf(topic));
            this.node.pubsub.ls().then((subscriptions: string[]) => {
                this.subscriptions = subscriptions;
            });
        } catch (err) {
            console.log("IFPS node not yet instantiated: ", err);
        }
    }
}
