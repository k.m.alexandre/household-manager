import { action, observable } from "mobx";
import { Receipt } from "@/types/calendar"
import { categoryColors } from "@/utils/categories"

export class ChartModel {
    @observable amountsPerCategory: Record<string, number>  = {};
    @observable labels: string[] = [];
    // @observable date: Date = new Date();
    @observable data: Array<number> = [];
    @observable total: number = 0;
    @observable colors: string[] = [];
    @observable private year: string = "2021"
    @observable private month: string = "07"

    @action.bound setExpenditureByCategory(receipts: Array<Receipt>) {
        const categories: string[] = [];
        this.labels = [];
        this.total = 0;
        this.colors = [];
        this.amountsPerCategory = {};
        this.data = [];
        for (const receipt of receipts) {
            const category: string = receipt.category;
            if (!categories.includes(category)) categories.push(category);
            if (!(receipt["category"] in this.amountsPerCategory))
                this.amountsPerCategory[category] = 0;
            this.amountsPerCategory[receipt["category"]] += receipt["amount"];
            this.total += receipt["amount"];
        }
        this.colors = categories.map(category => categoryColors[category]);
        this.labels = Object.keys(this.amountsPerCategory)
        for (const label of this.labels) {
            this.data.push(this.amountsPerCategory[label]);
        }
    }
}
