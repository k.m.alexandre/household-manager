import { action, computed, observable, toJS } from "mobx";
import { RootStore } from "@/store"
import { User } from "@/types/user"
import { decode } from "@/utils/message"

export type Message = {
    proto: string,
    id: string,
    name: string,
    content: string
}

export interface ReceiveData {
    (message: Uint8Array): void;
}

export class UserModel {
    root !: RootStore
    @observable _me!: User;
    @observable _users: User[] = Array<User>();
    @observable public _newUsers: User[] = []

    constructor(root: RootStore) {
        this.root = root;
    }

    @computed get newUsers() {
        return toJS(this._newUsers);
    }

    @computed get users() {
        return toJS(this._users);
    }

    @computed get userStore() {
        return this.root.ipfs.userStore;
    }

    @computed get nodeId() {
        return this.root.ipfs.nodeId;
    }

    @computed get node() {
        return this.root.ipfs.node;
    }

    @computed get me() {
        return toJS(this._me);
    }

    @action.bound setMe(me: User) {
        this._me = me;
    }

    @action.bound async initSelf() {
        const me = this.users.filter(user => {
            return user._id === this.nodeId;
        })
        if (me.length === 0) {
            const templateUser = { _id: this.nodeId, name: "Unnamed" };
            try {
                await this.userStore.put(templateUser);
                this.root.ipfs.subscribe(this.nodeId, this.receiveData.bind(this))
            } catch (err) {
                console.log("Initializing self user: ", err)
            }
        } else {
            this.setMe(me[0]);
        }
    }

    @action.bound async addUser(newUser: User) {
        console.log("ADD USER > ", newUser)
        const index = this.users.findIndex(user => user._id === newUser._id);
        if (index === -1) {
            this._users.push(newUser)
        } else {
            this._users[index] = newUser;
        }
        console.log(toJS(this.users))
        await this.userStore.put(newUser);
    }

    @action.bound removeNewUser(user: User) {
        this._newUsers = toJS(this.newUsers).filter(newUser => newUser._id !== user._id);
        console.log("REMOVE USER >> RESULT ", this._newUsers)
    }

    @action.bound async removeUser(userId: string) {
        await this.userStore.del(userId);
        this._users = this.users.filter(user => {
            return user._id !== userId });
        this.root.ipfs.unsubscribe(userId);
    }

    @action.bound async setUsers() {
        try {
            this._users = await this.userStore.all.map(data => {
                const user = data.payload.value
                this.root.ipfs.subscribe(user._id, this.receiveData.bind(this))
                return { _id: user._id, name: user.name } as User
            });
            if (this.users.length === 0) {
                const templateUser = { _id: this.nodeId, name: "Unnamed" };
                await this.addSelf(templateUser);
            }
        } catch (err) {
            alert(err);
            console.log(err);
        }
    }

    @action.bound async addSelf(user: User) {
        const message = { proto: 'updateUser', user };
        console.log(' UPDATE USER > ', user)
        await this.root.ipfs.publish(this.nodeId, message);
    }

    @action.bound async addPeer(peerId: string) {
        const topic = peerId
        try {
            console.log("Publishing to topic ", topic)
            const message = {
                proto: 'handshake',
                step: 'syn',
                id: this.nodeId
            };
            this.root.ipfs.publish(topic, message);
        } catch (err) {
            console.log("IFPS node not yet instantiated: ", err);
        }
    }

    public receiveData(message: Uint8Array) {
        // @ts-ignore
        const data = decode(message.data);
        console.log("RECEIVED DATA >>> ", data)
        switch (data.proto) {
            case 'handshake': {
                if (data.step === 'syn') {
                    const message = {
                        proto: 'handshake',
                        step: 'syn-ack',
                        id: this.nodeId,
                        name: this.me.name
                    };
                    console.log("Handshake: @syn ", data.id)
                    this.root.ipfs.publish(data.id, message)
                } else if (data.step === 'syn-ack') {
                    const message = {
                        proto: 'handshake',
                        step: 'ack',
                        id: this.nodeId,
                        name: this.me.name
                    };

                    this.root.ipfs.subscribe(
                        data.id, this.receiveData.bind(this));
                    this.root.ipfs.publish(data.id, message);
                    const index = this.users.findIndex(user => data.id === user._id);
                    // check duplicate
                    if (index === -1) {
                        this.addUser({ _id: data.id, name: data.name })
                    }
                } else if (data.step === 'ack') {
                    const index = this.users.findIndex(user => data.id === user._id);
                    // check duplicate
                    if (index === -1) {
                        this.addUser({ _id: data.id, name: data.name })
                    }
                    if (data.id !== this.node.id ) {
                        this.root.ipfs.subscribe(
                            data.id, this.receiveData.bind(this));
                    }
                }
                break;
            }
            case 'updateUser': {
                console.log("DATA > ", data)
                const user = data.user as User;
                console.log("Got new user data: ", user);
                this.addUser(user)
                break;
            }
            case 'replicate': {
                console.log(data.receipt)
                if (data.id !== this.nodeId)
                    this.root.ipfs.receipts.put(data.receipt);
            }
        }
    }
}
