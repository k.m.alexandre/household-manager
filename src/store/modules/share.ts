import { action, computed, observable } from "mobx";
import { RootStore } from "@/store"
import { PeerData } from "@/types/share";
import { Receipt } from "@/types/calendar";
import Store from "orbit-db-store";
const qrcode = require('qrcode-generator');



export class ShareModel {
    root: RootStore
    @observable _peerId: string = ""
    @observable receiptsDB: string = ""
    @observable showProgress: boolean = false
    @observable qrImage: string = ""
    @observable peerData: PeerData = {
        address: "",
        hash: "",
        entry: "",
        progress: "",
        have: ""
    }
    @observable isTimeOut: boolean = false;

    constructor(root: RootStore) {
        this.root = root;
    }

    @action.bound setShowProgress(showProgress: boolean) {
        this.showProgress = showProgress;
    }

    @action.bound setIsTimeOut(isTimeOut: boolean) {
        this.isTimeOut = isTimeOut;
    }

    @computed get localReceiptDB() {
        try {
            // @ts-ignore
            return this.root.ipfs.receipts.id;
        } catch (err) {
            console.log("IPFS node or DB not yet instantiated.");
            return "";
        }
    }

    @computed get peerId() {
        return this._peerId;
    }

    set peerId(peerId: string) {
        this._peerId = peerId;
    }

    @action.bound makeQR (id: string) {
        const ipnsLink = `https://gateway.ipfs.io/ipns/${id}`;
        // Build QR
        const qr = qrcode(0, 'L')
        qr.addData(ipnsLink)
        qr.make()

        // Create Base64
        let tag = qr.createSvgTag()
        tag = tag.replace('black', '#0b3a53', -1)
        const base64 = window.btoa(tag)

        console.log("base 64 ", base64)
        this.qrImage = base64;
    }

    @action.bound async replicatePeerDataWSS() {
        const receipts = await this.root.ipfs.receipts.all;
        for (let i = 0; i < receipts.length; i++) {
            console.log('receipt: ', receipts[i])
            const receipt = receipts[i].payload.value
            this.root.ipfs.publish(this.peerId, { proto: 'replicate', id: this.root.ipfs.nodeId, receipt });
            await new Promise(resolve => setTimeout(resolve, 500));
        }
    }

    @action.bound async replicatePeerDataIPFS() {
        this.isTimeOut = false;
        const db = this.peerId
        const peerDb = await this.root.ipfs.orbitdb.open(
            db, { create: true, type: 'docstore' });
        this.setShowProgress(true);
        peerDb.events.on('replicate', address => {
            console.log("REPLICATE FROM >>>> ", address)
        });
        peerDb.events.on('replicated', async () => {
            console.log("REPLICATED >>>> ", this.peerId)
            console.log("REPLICATED ALL >>> ", peerDb.all)
            // @ts-ignore
            peerDb.query((receipt: Receipt) => {
                console.log("RECEIPT ", receipt)
                this.root.ipfs.receipts.put(receipt);
                this.root.ipfs.setReplicated(true);
                return receipt
            })
            this.setShowProgress(false);
            this.setIsTimeOut(false);
        })
        peerDb.events.on("replicate.progress", (address, hash, entry, progress, have) => {
            this.isTimeOut = false;
            setTimeout(() => {
                this.setIsTimeOut(true);
                this.setShowProgress(false);
            }, 60000)
            console.log(entry)
            if (entry.payload.value === null) return;
            this.root.ipfs.receipts.put(entry.payload.value)
            this.peerData = { address, hash, entry: "", progress, have }
        })
        peerDb.events.on("peer", peer => {
            console.log("PEER >> ", peer)
        })
        peerDb.events.on("peer.exchanged", (peer, address, heads) => {
            console.log("PEER EXCHANGED >> >", peer, address, heads)
        })
    }
}
