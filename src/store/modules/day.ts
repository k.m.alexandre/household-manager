import { action, computed, observable } from "mobx";
import { RootStore } from "@/store"
import { Receipt } from "@/types/calendar"
import { expenditureTypes } from "@/utils/categories"

export class DayModel {
    @observable selectedExpenditureType: string = "";
    @observable amount: number = 0;
    @observable image: string = ""
    @observable receipts: Receipt[] = [];

    root: RootStore
    constructor(root: RootStore) {
      this.root = root;
    }

    @action.bound setSelectedExpenditureType(selectedExpenditureType: string) {
      this.selectedExpenditureType = selectedExpenditureType;
    }

    @action.bound setAmount(amount: number) {
      this.amount = amount;
    }

    @action.bound setReceipts(receipts: Receipt[]) {
      this.receipts = receipts;
    }

    @action.bound setImage(image: string) {
      this.image = image;
    }

    @action.bound removeImage() {
      this.image = "";
    }

    @action.bound addReceipt(receipt: Receipt) {
        this.receipts.push(receipt);
    }

    @action.bound resetNewReceipt() {
      this.selectedExpenditureType = "";
      this.amount = 0;
      this.image = "";
    }

    @action.bound async removeReceipt(index: number) {

      const removedReceipts = this.receipts.splice(index, 1);
      const removedReceipt = removedReceipts.pop();
      if (removedReceipt === undefined) return;
      this.root.ipfs.receipts.del(removedReceipt._id);
    }

    @action.bound fetchReceipts(start: number, end: number) {
        console.log("START > ", start, " END ", end)
        this.setReceipts(this.root.ipfs.receipts.query(receipt => {
            console.log("RECEIPT ", receipt)
            console.log(
            start <= receipt.date && receipt.date < end)
          return start <= receipt.date && receipt.date < end;
        }))
    }
}
