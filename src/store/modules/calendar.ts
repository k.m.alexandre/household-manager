import { action, computed, observable, toJS } from "mobx";
import { CalendarDate, Range, Receipt, ReceiptSummary } from "@/types/calendar"
import { RootStore } from "@/store"
import { formatAmount } from "@/utils/price"
import { categoryColors, expenditureTypesTranslated } from "@/utils/categories"
import { generateRange, getTimestamp } from "@/utils/date"

export class CalendarModel {
    root: RootStore
    @observable receipts = Array<Receipt>();
    @observable _receiptSummaries = Array<ReceiptSummary>();
    @observable receiptsByDay: Record<string, Array<Receipt>> = {};
    @observable lastStart!: CalendarDate;
    @observable lastEnd!: CalendarDate;
    @observable _range!: Range;
    @observable _stepType: string = "month";

    constructor(root: RootStore) {
        this.root = root;
        this.initializeRange();
    }

    @action.bound initializeRange() {
        this.setRange(generateRange(getTimestamp(), 0, this.stepType));
    }

    @computed get range() {
        return toJS(this._range);
    }

    @computed get receiptSummaries() {
        return toJS(this._receiptSummaries);
    }

    @computed get stepType() {
        return this._stepType;
    }

    set stepType(stepType: string) {
        this._stepType = stepType;
    }

    @computed get initialized() {
        return this.root.ipfs.initialized;
    }

    @action.bound setReceipt(receipt: Receipt) {
        this.receipts.push(receipt);
    }

    @action.bound setReceipts(receipts: Array<Receipt>) {
        this.receipts = receipts;
    }

    @action.bound setReceiptSummaries(receipts: Array<Receipt>) {
        this._receiptSummaries = receipts.map((receipt: Receipt) => {
            const creation = new Date(receipt.date);
            const category = receipt.category;
            const summary = {
                name: expenditureTypesTranslated[category] + ": " + formatAmount(receipt.amount),
                start: creation,
                end: creation,
                color: categoryColors[receipt.category as string]
            }
            return summary;
        });
    }

    @action.bound onChangeType(stepType: string) {
        this.stepType = stepType;
    }

    @action.bound changeRange(step: number) {
        const start = this.range.start;
        console.log("CHANGE RANGE > START > ", start, " STEP > ", step)
        this.setRange(generateRange(start, step, this.stepType));
    }

    @action.bound setRange(newRange: Range) {
        this._range = newRange;
        if (this.root.ipfs === undefined || !this.root.ipfs.initialized) {
            console.log("Range updated but DB not ready yet.");
            return;
        }

        try {
            const receipts = this.root.ipfs.receipts.query(
                (receipt: Receipt) => this._range.start <= receipt.date && receipt.date < this._range.end);
            this.setReceipts(receipts);
            this.setReceiptSummaries(receipts);
        } catch (err) {
            console.log("IPFS node not yet instantiated: ", err);
            this.setReceipts([]);
            this.setReceiptSummaries([]);
        }
    }

    @action.bound async fetchReceipts() {
        if (this.root.ipfs.receipts === undefined) {
            console.log("DB not defined");
            return;
        }
        for (const receipt of this.receipts) {
            console.log(receipt.date)
            const day = new Date(receipt.date);
            const yyyymmdd = `${day.getFullYear()}-${("0" + (day.getMonth() + 1)).slice(-2)}-${("0" + (day.getDay() + 1)).slice(-2)}`;
            console.log("Fetch receipts : receipt ", yyyymmdd)
            if (!(yyyymmdd in this.receiptsByDay)) this.receiptsByDay[yyyymmdd] = [];
            this.receiptsByDay[yyyymmdd].push(receipt);
        }
        return true;
    }
}
