import { action, computed, observable } from "mobx";
import { UserModel } from "@/store/modules/user"
import { User } from "@/types/user"

export class SettingsModel {
    userModel !: UserModel
    @observable _name: string = "";
    @observable _id: string = "";

    constructor(userModel: UserModel) {
        this.userModel = userModel;
    }

    init() {
        if (this.userModel.me !== undefined) {
            this._name = this.userModel.me.name;
            this._id = this.userModel.me._id;
        }
    }

    @computed get id() {
        return this._id;
    }

    set id(id: string) {
        this._id = id;
    }

    @computed get name() {
        return this._name;
    }

    get user() {
        return { _id: this.id, name: this.name } as User;
    }

    set name(name: string) {
        this._name = name;
    }

    async saveSelf() {
        if (this.id !== "" && this.name !== "")
            this.userModel.addSelf(this.user);
    }

    async addPeer(peerId: string) {
        if (peerId !== "")
            this.userModel.addPeer(peerId);
    }
}

