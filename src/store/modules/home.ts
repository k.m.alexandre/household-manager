import { action, observable } from "mobx";
import { CalendarDate } from "@/types/calendar"


export class HomeModel {
    @observable lastStart!: CalendarDate;
    @observable lastEnd!: CalendarDate;

    @action.bound setLastStart(lastStart: CalendarDate) {
        this.lastStart = lastStart;
    }

    @action.bound setLastEnd(lastEnd: CalendarDate) {
        this.lastEnd = lastEnd;
    }
}