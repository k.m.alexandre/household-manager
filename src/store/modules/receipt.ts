import { action, observable } from "mobx";
import { RootStore } from "@/store";
import { Receipt } from "@/types/calendar";
const Hash = require("ipfs-only-hash");

export class ReceiptModel {
    root: RootStore
    @observable selectedExpenditureType: string = "";
    @observable amount: string = "";
    // @observable date: Date = new Date();
    @observable image: string = ""
    @observable receipts = Array<Receipt>();

    constructor(root: RootStore) {
      this.root = root;
    }

    @action.bound setSelectedExpenditureType(selectedExpenditureType: string) {
      this.selectedExpenditureType = selectedExpenditureType;
    }

    @action.bound setAmount(amount: string) {
      this.amount = amount;
    }

    // @action.bound setDate(date: Date) {
    //   this.date = date;
    // }

    @action.bound setImage(image: string) {
      this.image = image;
    }

    @action.bound removeImage() {
      this.image = "";
    }

    @action.bound resetNewReceipt() {
      this.selectedExpenditureType = "";
      this.amount = "";
      // this.date = new Date();
      this.image = "";
    }

    @action.bound async saveReceipt(day: number) {
      const hash = await Hash.of(this.image)
      const receipt = {
          _id: hash,
          image: this.image,
          amount: Number(this.amount),
          category: this.selectedExpenditureType,
          date: day
      };
      await this.root.ipfs.receipts.put(receipt);
      this.resetNewReceipt();
      return receipt;
    }
}
