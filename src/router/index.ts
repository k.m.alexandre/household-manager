import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
      path: '/',
      name: 'landing',
      component: () => import('@/views/Landing.vue'),
      redirect: '/calendar',
      children: [
          {
              path: 'settings',
              name: 'settings',
              component: () => import('@/views/Settings.vue')
          },
          {
              path: 'calendar',
              name: 'calendar',
              component: () => import('@/views/Calendar.vue'),
              props: true
          },
          {
              path: 'day/:day',
              name: 'day',
              component: () => import('@/views/Day.vue'),
              props: true
          },
          {
              path: 'share',
              name: 'share',
              component: () => import('@/views/Share.vue'),
              props: true
          },
          {
              path: 'chat',
              name: 'chat',
              component: () => import('@/views/Chat.vue'),
              props: true
          },
          {
              path: 'chart',
              name: 'chart',
              component: () => import('@/views/Overview.vue')
          }
      ]
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('@/views/Home.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
